from flask import Flask
from flask import request
from random import uniform
from time import sleep


app = Flask(__name__)


@app.route('/')
def hello_world():
    r_id = request.args.get('r_id')
    sleep(uniform(0, 0.5))
    print(f'received {r_id}')
    return {'status': True}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
